import { LOAD_USER_DATA, DATA_FETCHING_START, DATA_FETCHING_STOP, ERROR_USER_LOGIN  } from '../constants/user';
import { UserAction } from '../actions/user';
import { User } from '../interfaces/models/User';

export interface UserState {
    user?: User;
    errors?: string;
    dataFetching: boolean;
}

export interface UserDispatch {
    fetchUserToken: (login: string, password: string) => void;
    fetchUserData: () => void;
}

const initState: UserState = {
    user: undefined,
    errors: undefined,
    dataFetching: false
};

function user(state: UserState = initState, action: UserAction) {
    switch (action.type) {
        case LOAD_USER_DATA:
            return {...state, user: action.user};
        case DATA_FETCHING_START:
            return {...state, dataFetching: true};
        case DATA_FETCHING_STOP:
            return {...state, dataFetching: false};
        case ERROR_USER_LOGIN:
            return {...state, errors: action.errors};
        default:
            return state;
    }
}

export default user;