import { expect } from 'chai';
import * as faker from 'faker';

import user, { UserState } from '../user';
import * as types from '../../constants/user';
import { UserAction } from '../../actions/user';
import { User } from '../../interfaces/models/User';

describe('user reducers', () => {
    it('should return init state', () => { 
        const action: UserAction = {
            type: 'UNDEFINED_TYPE'
        };
        const initState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: false
        };
        const expectState = initState;

        expect(user(initState, action)).to.be.deep.equal(expectState);
    });

    it('should change data fetching flag to true', () => { 
        const action: UserAction = {
            type: types.DATA_FETCHING_START    
        };
        const initState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: false
        };
        const expectState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: true
        };

        expect(user(initState, action)).to.be.deep.equal(expectState);
    });

    it('should change data fetching flag to false', () => { 
        const action: UserAction = {
            type: types.DATA_FETCHING_STOP    
        };
        const initState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: true
        };
        const expectState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: false
        };

        expect(user(initState, action)).to.be.deep.equal(expectState);
    });

    it('should handle user data', () => { 
        const loadedUser: User = {
            id: faker.random.number(),
            name: faker.name.firstName(),
            email: faker.internet.email(),
        };
        const action: UserAction = {
            type: types.LOAD_USER_DATA,
            user: loadedUser
        };
        const initState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: false
        };
        const expectState: UserState = {
            user: loadedUser,
            errors: undefined,
            dataFetching: false
        };

        expect(user(initState, action)).to.be.deep.equal(expectState);
    });

    it('should handle user login errors', () => { 
        const errors = faker.random.words();
        const action: UserAction = {
            type: types.ERROR_USER_LOGIN,
            errors
        };
        const initState: UserState = {
            user: undefined,
            errors: undefined,
            dataFetching: false
        };
        const expectState: UserState = {
            user: undefined,
            errors,
            dataFetching: false
        };

        expect(user(initState, action)).to.be.deep.equal(expectState);
    });
});