import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as React from 'react';

import LoginForm from '../../components/LoginForm/LoginForm';
import Profile from '../../components/Profile/Profile';
import { fetchUser, fetchToken } from '../../actions/user';
import { UserState, UserDispatch } from '../../reducers/user';
import { RootState } from '../../reducers/index';

class LoginContainer extends React.PureComponent<UserState & UserDispatch, {}> {
    render() {
        let output = (
            <LoginForm
                errors={this.props.errors}
                dataFetching={this.props.dataFetching}
                fetchUserToken={this.props.fetchUserToken}
            />
        );

        if (this.props.user) {
            output = <Profile user={this.props.user} />;
        }

        return output;
    }

    componentWillMount() {
        if (sessionStorage.getItem('token')) {
            this.props.fetchUserData();
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);

function mapStateToProps(state: RootState): UserState {
    return {
        user: state.user.user,
        errors: state.user.errors,
        dataFetching: state.user.dataFetching
    };
}

function mapDispatchToProps(dispatch: Dispatch<UserDispatch>): UserDispatch {
    return {
        fetchUserToken: (email: string, password: string) => {
            dispatch(fetchToken(email, password));
        },
        fetchUserData: () => {
            dispatch(fetchUser());
        }
    };
}