import * as React from 'react';

import './Loader.css';

const loader = require('../../gamepad.png');

class Loader extends React.PureComponent {
    render() {
        return <img className="loader" src={loader} />; 
    }
}

export default Loader;