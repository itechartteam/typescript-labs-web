import { shallow } from 'enzyme';
import * as React from 'react';
import { expect } from 'chai';

import Loader from './Loader';

describe('Loader component', () => {
    it('should render loader', () => { 
        const loader = shallow(<Loader />);
        expect(loader.find('img').length).to.be.equal(1);
    });
});