import { shallow } from 'enzyme';
import * as React from 'react';
import { expect } from 'chai';
import * as faker from 'faker';

import LoginForm from './LoginForm';
import Loader from '../Loader/Loader';

describe('LoginForm component', () => {
    const emptyFetch = () => {
        return;
    };

    it('shoul render component', () => { 
        const loginForm = shallow(<LoginForm errors="" dataFetching={false} fetchUserToken={emptyFetch}/>);

        expect(loginForm.find({name: 'email'}).length).to.be.equal(1); 
        expect(loginForm.find({name: 'password'}).length).to.be.equal(1); 
        expect(loginForm.find('button').length).to.be.equal(1); 
        expect(loginForm.find('.error-container').text()).to.be.equal('');
    });

    it('shoul render component with errors', () => { 
        const errors = faker.random.words();
        const loginForm = shallow(<LoginForm errors={errors} dataFetching={false} fetchUserToken={emptyFetch}/>);

        expect(loginForm.find({name: 'email'}).length).to.be.equal(1); 
        expect(loginForm.find({name: 'password'}).length).to.be.equal(1); 
        expect(loginForm.find('button').length).to.be.equal(1); 
        expect(loginForm.find('.error-container').text()).to.be.equal(errors);
    });

    it('shoul render loader', () => { 
        const loginForm = shallow(<LoginForm errors="" dataFetching={true} fetchUserToken={emptyFetch}/>);

        expect(loginForm.contains(<Loader />)).to.be.equal(true); 
    });

    it('should update email state on change', () => {
        const email = faker.internet.email();
        const loginForm = shallow(<LoginForm errors="" dataFetching={false} fetchUserToken={emptyFetch}/>);

        loginForm.find({name: 'email'}).simulate('change', { currentTarget: { value: email }});
        expect(loginForm.state().email).to.be.equal(email);
    });

    it('should update password state on change', () => {
        const password = faker.random.alphaNumeric();
        const loginForm = shallow(<LoginForm errors="" dataFetching={false} fetchUserToken={emptyFetch}/>);

        loginForm.find({name: 'password'}).simulate('change', { currentTarget: { value: password }});
        expect(loginForm.state().password).to.be.equal(password);
    });

    it('should handle fetchUserToken function on click button', () => {
        const mockFetchFunction = () => {
            loginForm.state().test = true;
        };
        const loginForm = shallow(<LoginForm errors="" dataFetching={false} fetchUserToken={mockFetchFunction}/>);

        loginForm.find('button').simulate('click');
        expect(loginForm.state().test).to.be.equal(true);
    });
});