import * as React from 'react';

import Loader from '../Loader/Loader';

interface LoginProps {
    errors?: string;
    dataFetching: boolean;
    fetchUserToken: (login: string, password: string) => void;
}

interface LoginState {
    email: string;
    password: string;
}

class LoginForm extends React.PureComponent<LoginProps, LoginState> {
    constructor(props: LoginProps) {
        super(props);

        this.state = {
            email: '',
            password: ''
        };
    }

    handleClick = () => {
        this.props.fetchUserToken(this.state.email, this.state.password);
    }

    handleEmailUpdate = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({email: event.currentTarget.value });
    }

    handlePasswordUpdate = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({password: event.currentTarget.value });
    }

    render() {
        const { dataFetching } = this.props;

        let output = (
            <div>
                <input 
                    type="text" 
                    placeholder="email" 
                    name="email" 
                    value={this.state.email}
                    onChange={this.handleEmailUpdate} 
                />
                <input 
                    type="password" 
                    placeholder="password" 
                    name="password" 
                    value={this.state.password}
                    onChange={this.handlePasswordUpdate} 
                />
                <button onClick={this.handleClick}>Login</button>
                <div className="error-container">
                    {this.props.errors}
                </div>
            </div>
        );

        if (dataFetching) {
            output = <Loader />;
        }
        
        return output;
    }
}

export default LoginForm;