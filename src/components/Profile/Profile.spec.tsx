import { shallow } from 'enzyme';
import * as React from 'react';
import { expect } from 'chai';
import * as faker from 'faker';

import Profile from './Profile';
import { User } from '../../interfaces/models/User';

describe('Profile component', () => {
    it('shoul render component', () => { 
        const user: User = {
            id: faker.random.number(),
            email: faker.internet.email(),
            name: faker.name.firstName()
        };
        const profile = shallow(<Profile user={user} />);

        expect(profile.find('.profile-container').length).to.be.equal(1); 
    });
});