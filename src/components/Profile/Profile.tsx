import * as React from 'react';

import { User } from '../../interfaces/models/User';

interface ProfileProps {
    user: User;
}

class Profile extends React.PureComponent<ProfileProps> {

    render() {
        return(
            <div className="profile-container">
                <div>Email: {this.props.user.email}</div>
                <div>Name: {this.props.user.name}</div>
            </div>
        );
    }

}

export default Profile; 