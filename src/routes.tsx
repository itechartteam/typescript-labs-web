import * as React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import App from './components/App';
import Login from './containers/LoginContainer/LoginContainer';

const Routes = () => (
    <BrowserRouter>
        <App>
            <Switch>
                <Route component={Login} path="/login" />
            </Switch>
        </App>
    </BrowserRouter>
);

export default Routes;