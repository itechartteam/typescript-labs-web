import { Dispatch } from 'redux';

import { User } from '../interfaces/models/User';
import { UserDispatch } from '../reducers/user';
import apiProvider from '../services/apiProvider';
import { DATA_FETCHING_START, DATA_FETCHING_STOP, ERROR_USER_LOGIN, LOAD_USER_DATA } from '../constants/user';

interface LoadUserData {
    type: string;
    user?: User;
}

interface DataFetching {
    type: string;
}

interface ErrorUserLogin {
    type: string;
    errors?: string;
}

export type UserAction = LoadUserData & ErrorUserLogin & DataFetching;

export function loadUserAction(user: User): LoadUserData {
    return {
        type: LOAD_USER_DATA,
        user
    };
}

export function dataFetchingStartAction(): DataFetching {
    return {
        type: DATA_FETCHING_START
    };
}

export function dataFetchingStopAction(): DataFetching {
    return {
        type: DATA_FETCHING_STOP
    };
}

export function errorUserLoginAction(errors: string): ErrorUserLogin {
    return {
        type: ERROR_USER_LOGIN,
        errors
    };
}

export const fetchUser = () => {
    return async (dispatch: Dispatch<UserDispatch>) => {
        try {
            const response: Response = await apiProvider.get(
                '/user/me',
                {
                    dispatch: dispatch,
                    onFetchingEndAction: dataFetchingStopAction,
                    onFetchingStartAction: dataFetchingStartAction
                }
            );
            const responseData = await response.json();
            
            if (response.status !== 200) {
                throw new Error(responseData.message);
            }
            
            const responseUser = responseData.user;
            const user =  {
                id: responseUser.id,
                name: responseUser.name,
                email: responseUser.email
            };
    
            dispatch(loadUserAction(user));
        } catch (error) {
            dispatch(errorUserLoginAction(error.message));
            // TODO: change errors formating
            return;
        }
    };
};

export const fetchToken = (email: string, password: string) => {
    return async (dispatch: Dispatch<UserDispatch>) => {
        try {
            const response: Response | Error = await apiProvider.post(
                '/user/login',
                { email, password },
                {
                    dispatch: dispatch,
                    onFetchingEndAction: dataFetchingStopAction,
                    onFetchingStartAction: dataFetchingStartAction
                }
            );
            const responseData = await response.json();
        
            if (response.status !== 200) {
                throw new Error(responseData.message);
            }
        
            const token = responseData.token;
            sessionStorage.setItem('token', token);
            dispatch(fetchUser());
        } catch (error) {
            dispatch(errorUserLoginAction(error.message));
             // TODO: change errors formating
            return;
        }
    };
};
