import { expect } from 'chai';
import * as faker from 'faker';
import * as nock from 'nock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as sinon from 'sinon';

import * as actions from '../../actions/user';
import * as types from '../../constants/user';
import { User } from '../../interfaces/models/User';
import { API_HOST } from '../../configs/api';
import apiProvider from '../../services/apiProvider';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('user actions', () => {
    afterEach(() => {
        nock.cleanAll();
    });

    it('should create action to set data fetching in true', () => {
        const expectedAction = {
            type: types.DATA_FETCHING_START
        };

        expect(actions.dataFetchingStartAction()).to.be.deep.equal(expectedAction);
    });

    it('should create action to set data fetching in false', () => {
        const expectedAction = {
            type: types.DATA_FETCHING_STOP
        };

        expect(actions.dataFetchingStopAction()).to.be.deep.equal(expectedAction);
    });

    it('should create action to load user data', () => {
        const user: User = {
            id: faker.random.number(),
            name: faker.name.firstName(),
            email: faker.internet.email(),
        };
        const expectedAction = {
            type: types.LOAD_USER_DATA,
            user
        };

        expect(actions.loadUserAction(user)).to.be.deep.equal(expectedAction);
    });

    it('should create action to load login error', () => {
        const errors = faker.random.words();
        const expectedAction = {
            type: types.ERROR_USER_LOGIN,
            errors
        };

        expect(actions.errorUserLoginAction(errors)).to.be.deep.equal(expectedAction);
    });

    it('should create LOAD_USER_DATA when user fetching', () => {
        const user: User = {
            id: faker.random.number(),
            name: faker.name.firstName(),
            email: faker.internet.email()
        };

        sinon.stub(apiProvider, 'configureHttpOptions' as any).callsFake(() => {
            return {};
        });

        nock(API_HOST)
            .get('/user/me')
            .reply(200, { user }, {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            });

        const expectedActions = [
            { type: types.DATA_FETCHING_START },
            { type: types.DATA_FETCHING_STOP },
            { type: types.LOAD_USER_DATA, user }
        ];
        const store = mockStore({});

        return store.dispatch(actions.fetchUser()).then(() => {
            expect(store.getActions()).to.be.deep.equal(expectedActions);
        });
    });
});