import { API_HOST } from '../configs/api';

type methods = 'POST' | 'GET' | 'DELETE' | 'PUT' | 'PATCH' | 'OPTION';

interface HttpOptionInterface {
    method: methods;
    headers: Object;
    body?: string;
}

interface DispatchOptionsInterface {
    dispatch: Function;
    onFetchingStartAction: Function;
    onFetchingEndAction: Function;
}

class ApiProvider {
    static get(
        url: string, dispatchOptions?: DispatchOptionsInterface, params?: Object
    ): Promise<Response> {
        return ApiProvider.fetchHttp('GET', url, dispatchOptions, undefined, params);
    }
    
    static post(
        url: string, body: Object, dispatchOptions?: DispatchOptionsInterface, params?: Object
    ): Promise<Response> {
        return ApiProvider.fetchHttp('POST', url, dispatchOptions, body, params);
    }
    
    static remove(
        url: string, dispatchOptions?: DispatchOptionsInterface, params?: Object
    ): Promise<Response> {
        return ApiProvider.fetchHttp('DELETE', url, dispatchOptions, undefined, params);
    }
    
    static put(
        url: string, 
        body: Object, 
        dispatchOptions?: DispatchOptionsInterface, 
        params?: Object
    ): Promise<Response> {
        return ApiProvider.fetchHttp('PUT', url, dispatchOptions, body, params);
    }
    
    static patch(
        url: string, 
        body: Object, 
        dispatchOptions?: DispatchOptionsInterface,
        params?: Object
    ): Promise<Response> {
        return ApiProvider.fetchHttp('PATCH', url, dispatchOptions, body, params);
    }

    private static async fetchHttp(
        method: methods, 
        url: string, 
        dispatchOptions?: DispatchOptionsInterface,
        body?: Object, 
        params?: Object
    ): Promise<Response> {
        const httpOptions = ApiProvider.configureHttpOptions(method, body);

        if (params) {
            url = ApiProvider.bindParams(url, params);
        }

        if (!dispatchOptions) {
            return ApiProvider.fetchWithoutDispatch(url, httpOptions);
        }

        return ApiProvider.fetchWithDispatch(url, httpOptions, dispatchOptions);
    }

    private static async fetchWithDispatch(
        url: string, 
        httpOptions: Object, 
        dispatchOptions: DispatchOptionsInterface
    ): Promise<Response> {
        dispatchOptions.dispatch(dispatchOptions.onFetchingStartAction());
        const response = await fetch(API_HOST + url, httpOptions);
        dispatchOptions.dispatch(dispatchOptions.onFetchingEndAction());

        return response;
    }

    private static fetchWithoutDispatch(url: string, httpOptions: Object) {
        return fetch(API_HOST + url, httpOptions);
    }

    private static configureHttpOptions(method: methods, body?: Object): HttpOptionInterface {
        let httpOptions: HttpOptionInterface = {
            method: method,
            headers: {
                'content-type': 'application/json'
            }
        };
    
        const userToken = sessionStorage.getItem('token');
        if (userToken) {
            httpOptions.headers['x-access-token'] = userToken;
        }
    
        if (body) {
            httpOptions.body = JSON.stringify(body);
        }
    
        return httpOptions;
    }

    private static bindParams(url: string, params: Object): string {
        Object.keys(params).map((key) => {
            url = url.replace(new RegExp('(^|\/):' + key + '(\/|$)', 'g'), '$1' + params[key] + '$2');
        });
        
        return url;
    }
} 

export default ApiProvider;